﻿//Sucesión de Fibonacci

int N, NUM1 = 0, NUM2 = 1, NUMX;

Console.Write("Dijite el valor (posición) a calcular: ");
N = Convert.ToInt32(Console.ReadLine());

for (int i = 1; i <= N; i++)
{
    if (i <= 1)
    {
        NUMX = i;
    }
    else
    {
        NUMX = NUM1 + NUM2;
        NUM1 = NUM2;
        NUM2 = NUMX;
    }

    Console.Write("{0} - ", NUMX);
}
Console.WriteLine();
